var bs = require ('../src/bubbleSort.js');

function bubbleSortTest () {

    var passed = true;

    var unsorted = [2389, 283, 20, 5470, 892, 671438, 12, 0923, 7901564, 78];
    var sorted = [12, 20, 78, 283, 892, 923, 2389, 5470, 671438, 7901564];

    // Use BubbleSort to sort the unsorted array
    var bubbleSorted = bs.bubbleSort (unsorted);

    // First test if both arrays are of equal length
    if (bubbleSorted.length != sorted.length) {
        console.log ("Arrays are of unequal length");
        passed = false;
    }

    // Next test if all the members equal each other
    for (var i = 0; i < bubbleSorted.length; i++) {
        if (bubbleSorted[i] != sorted[i]) {
            console.log ("Values at index " + i + " do not match!");
            passed = false;
        }
    }

    if (passed) {
        console.log ("All tests passed.");
    } else {
        console.log ("Tests failed.");
    }
}

bubbleSortTest ();