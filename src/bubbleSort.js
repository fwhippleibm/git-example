
// Adapted from:  http://blog.benoitvallon.com/sorting-algorithms-in-javascript/the-bubble-sort-algorithm/

// swap function helper
function swap(array, i, j) {
  var temp = array[i];
  array[i] = array[j];
  array[j] = temp;
}

// This function needs to be documented!
function bubbleSort(array) {
  var swapped;
  do {
    swapped = false;
    for(var i = 0; i < array.length; i++) {
      if(array[i] && array[i + 1] && array[i] > array[i + 1]) {
        swap(array, i, i + 1);
        swapped = true;
      }
    }
  } while(swapped);
  return array;
}

module.exports.bubbleSort = bubbleSort;
  
  